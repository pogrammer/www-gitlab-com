---
layout: markdown_page
title: "Category Direction - Groups & Projects"
description: "Groups are a fundamental building block (a small primitive) in GitLab for project organization and managing access to these resources at scale. Learn more!"
canonical_path: "/direction/core_platform/tenant-scale/groups-&-projects/"
---

- TOC
{:toc}

## Groups & Projects

| | |
| --- | --- |
| Stage | [Data Stores](https://about.gitlab.com/direction/core_platform/) |
| Maturity | [Complete](https://about.gitlab.com/direction/#maturity) |
| Content Last Reviewed | `2024-08-20` |

## Introduction and how you can help
Thanks for visiting this category direction page on Groups & Projects at GitLab. The Groups & Projects category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Core Platform](https://about.gitlab.com/direction/core_platform/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Atenant%20scale&label_name%5B%5D=Category%3AGroups%20%26%20Projects&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::tenant+scale&label_name[]=Category:Groups+&+Projects). Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min). If you're a GitLab user and have direct feedback about your needs for Groups & Projects, we'd love to hear from you.
* Please open an issue using the ~"Category:Groups & Projects" label, or reach out to us internally in the #g_tenant-scale Slack channel.

## Overview

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives)) in GitLab that serve to:
- Group users to manage project authorization at scale
- Create collections of users for security features like [code owners](https://docs.gitlab.com/ee/user/project/codeowners/#code-owners) and [protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protected-environments)
- Organize related projects together
- House features that cover multiple projects like epics, contribution analytics, and the security dashboard

## Strategy and Themes

In 2024, our goal is to make improvements by extending Groups & Projects to help enterprise organizations thrive on GitLab.com.
We will accomplish that by iterating on existing constructs and streamlining workflows to guide users to intended usage patterns.

### Opportunities

Iterating on Groups & Projects will allow us to solve a number of problems with the current experience:
- **Flexible hierarchies** - We need to offer additional configuration options to allow enterprises to represent different [types of organizations](https://creately.com/blog/diagrams/types-of-organizational-charts/) using groups, subgroups and projects.
- **Organizing content** - Particularly during the creation process, users experience confusion around the nesting of projects within groups. We want to increase usability in this area.
- **Discoverability** - Users currently have difficulties finding relevant content across the product, for example on list pages (including project, group, personal projects, and activity lists). At the same time, mechanisms to discover and explore new content are limited.
- **Deletion and recovery** - Project and group deletion and recovery are currently inconsistent and need to be improved.
- **Archiving** - Not all content can be archived and, once archived, the display and messaging of that content needs improvement.

## 1 year plan

Over the next year, we are aiming to unify and standardize the group and project listing UI by implementing consistent filtering options and migrating components to Vue.

### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->
The Tenant Scale group is currently focusing on the [Organization](https://about.gitlab.com/direction/core_platform/tenant-scale/organization/) and [Cell](https://about.gitlab.com/direction/core_platform/tenant-scale/cell/) categories.
This means that we are currently only considering improvements to Groups & Projects that benefit the maturity of either of these categories. 

We want to make existing project functionality available at the group level. The first iterations of this will include:
- [Making archiving available at the group level](https://gitlab.com/gitlab-org/gitlab/-/issues/382051)

### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- [Improve source column in membership table](https://gitlab.com/gitlab-org/gitlab/-/issues/431066)
- [Ensure group members from shared groups are displayed on the members tab](https://gitlab.com/gitlab-org/gitlab/-/issues/429341)

### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

#### Completed in 17.3

- [Modernize sorting and filtering in Your Work > Projects](https://gitlab.com/gitlab-org/gitlab/-/issues/25368)
- [Pending deletion message is different between groups and projects](https://gitlab.com/gitlab-org/gitlab/-/issues/397032)

#### Completed in 17.2

- [Modernize sorting and filtering in Group overview](https://gitlab.com/gitlab-org/gitlab/-/issues/437013)
- [REST API Endpoint for groups that a group was invited to](https://gitlab.com/gitlab-org/gitlab/-/issues/424959)

#### Completed in 17.1

- [Modernize sorting and filtering in Explore > Projects](https://gitlab.com/gitlab-org/gitlab/-/issues/434473)
- [Members API: Allow adding member by Username](https://gitlab.com/gitlab-org/gitlab/-/issues/28208)
- [Create a GraphQL query for contributed projects](https://gitlab.com/gitlab-org/gitlab/-/issues/450191)
- [Groups API: Filter groups by marked for deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/429315)
- [Add variable %{latest_tag} available for badge link and url](https://gitlab.com/gitlab-org/gitlab/-/issues/26420)
- [Projects API: Filter projects by marked for deletion date](https://gitlab.com/gitlab-org/gitlab/-/issues/463939)
- [Add a way to filter projects by date they were marked for deletion with GraphQL](https://gitlab.com/gitlab-org/gitlab/-/issues/463809)
- [Add new placeholders for group/project badges](https://gitlab.com/gitlab-org/gitlab/-/issues/22278)

### What is not planned right now

* The Tenant Scale team will not be responsible for migrating all features from a group or project to a namespace. We are [building a framework](https://gitlab.com/groups/gitlab-org/-/epics/6473) and are [documenting the process](https://docs.gitlab.com/ee/development/organization/index.html#consolidate-groups-and-projects) for features to be migrated. We collaborate with the respective teams to migrate existing functionality to namespaces.

## Target Audience
Groups and Projects are used by all GitLab users, no matter what role. However, there are some heavier users of Group and Project functionality: 

* [Sidney (Systems Adminstrator)](https://about.gitlab.com/handbook/product/personas/#sidney-systems-administrator) is often overseeing the GitLab implementation of their organization and manages access to Groups and Projects. Sidney is often involved in setting up and maintaining a Project and Group hierarchy that best represents the organizational structure of the company they work at.
* [Delaney (Development Team Lead)](https://about.gitlab.com/handbook/product/personas/#delaney-development-team-lead) is primarily managing and coordinating the development team efforts within their team. Delaney often manages access to a specific subset of Groups and Projects representing their team or department.
* [Sasha (Software Developer)](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer) is primarily writing code, but uses Groups and Projects to collaborate with team mates. Sasha needs to understand who the members of a specific Group or Project are to involve the right team members when submitting merge requests or reviewing code.

## Top user issue(s)

Here is a list of the [top user upvoted issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=popularity&state=opened&label_name%5B%5D=group%3A%3Atenant%20scale&label_name%5B%5D=Category%3AGroups%20%26%20Projects&first_page_size=100) with descending popularity:
* [Create an option to archive/unarchive Groups](https://gitlab.com/gitlab-org/gitlab/-/issues/15967)
* [Share private project via URL](https://gitlab.com/gitlab-org/gitlab/-/issues/15549)
* [Add support for subgroups in personal namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/16734)
