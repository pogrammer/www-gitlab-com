Thanks for your interest in becoming a database reviewer! Database reviewers are like backend reviewers but with a special focus on the database. As a result, there are some special challenges they may face in the course of their necessary work.

Before becoming a database reviewer, we ask that you complete the following steps to be sure you're prepared!

## New reviewer tasks
- [ ] Change merge request to include your name: `Add Database Reviewer: Your Name`
- [ ] Review general [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html)
- [ ] Familiarize yourself with [how to review for database](https://docs.gitlab.com/ee/development/database_review.html#how-to-review-for-database)
- [ ] Ask to be added to `@gl-database`, this handle is used to inform and consult with database folks about updates to the database review process.
- [ ] Read [Understanding EXPLAIN plans](https://docs.gitlab.com/ee/development/database/understanding_explain_plans.html)
- [ ] Read up on what to do if you're feeling overwhelmed by [database reviews](https://docs.gitlab.com/ee/development/database/database_reviewer_guidelines.html#what-to-do-if-you-feel-overwhelmed)
- [ ] Optional (but recommended): Open an access request for (if you don't have them already):
  - [ ] `AllFeaturesUser` access in postgres.ai
  - [ ] The `db-lab` chef role for psql access to database lab
  - [ ] `Developer` role in the [ops testing pipeline project](https://ops.gitlab.net/gitlab-com/database-team/gitlab-com-database-testing/).
- [ ] Mention the [database team manager](https://about.gitlab.com/handbook/engineering/infrastructure/core-platform/data_stores/database/) for awareness
- [ ] Assign the issue to your manager to merge

Note that *approving and accepting* merge requests is *restricted* to
Database Maintainers only. As a reviewer, pass the MR to a maintainer
for approval.

/label ~Database

## Where to go for questions?

Reach out to `#database` on Slack
