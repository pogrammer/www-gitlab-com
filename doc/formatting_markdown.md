## Formatting markdown

The `yarn format-markdown` command formats a markdown file adhering to a set of
formatting preferences described in the sections below. The purpose of the formatter
is enforcing a single markdown formatting style across all markdown files in this
project. To format a file, specify the file path using the `-p` flag, for example:

```
yarn format-markdown -p sites/handbook/source/handbook/communication/index.md
```

See the [markdown guide](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/uncategorized/source/community/markdown-guide-middleman/index.html.md) for how to format markdown.