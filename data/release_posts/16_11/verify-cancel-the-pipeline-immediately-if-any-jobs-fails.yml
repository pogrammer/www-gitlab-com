---
features:
  primary:
  - name: "Option to cancel a pipeline immediately if any jobs fails"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/#workflowauto_cancelon_job_failure'
    reporter: rutshah
    stage: verify
    categories: [Continuous Integration (CI)]
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/23605'
    image_url: '/images/16_11/16.11_auto_cancel_on_job_failure.png'
    description: |
      Sometimes after you notice a job fails, you might manually cancel the rest of the pipeline to save resources while you work on the issue causing the failure. With GitLab 16.11, you can now configure pipelines to be cancelled automatically when any job fails. With large pipelines that take a long time to run, especially with many long-running jobs that run in parallel, this can be an effective way to reduce resource usage and costs.

      You can even configure a pipeline to immediately [cancel if a downstream pipeline fails](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#auto-cancel-the-parent-pipeline-from-a-downstream-pipeline), which cancels the parent pipeline and all other downstream pipelines.

      Special thanks to [Marco](https://gitlab.com/zillemarco) for contributing to the feature!
