---
features:
  secondary:
  - name: New contributor count metric in the Value Streams Dashboard
    available_in: [gold]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#devsecops-metrics-comparison-panel'
    image_url: '/images/16_10/vsd_ca2.png'
    reporter: hsnir1
    stage: plan
    categories: [Value Stream Management]
    issue_url: https://gitlab.com/gitlab-org/gitlab/-/issues/433353
    description: |
      To enable software leaders to gain insights into the relationship between team velocity, software stability, security exposures, and team productivity, we introduced a new [**Contributor count** metric in the Value Streams Dashboard](https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html#dashboard-metrics-and-drill-down-reports). The contributor count represents the number of monthly unique users with contributions in the group. This metric is designed to track adoption trends over time, and is based on [contributions calendar events](https://docs.gitlab.com/ee/user/profile/contributions_calendar.html#user-contribution-events). 
      
      The **Contributor count** metric is available only on GitLab.com, and requires the [contribution analytics report to be configured to run through ClickHouse](https://docs.gitlab.com/ee/user/group/contribution_analytics/#contribution-analytics-with-clickhouse). [Issue 441626](https://gitlab.com/gitlab-org/gitlab/-/issues/441626) tracks efforts to make this feature available to self-managed customers as well.
