---
features:
  secondary:
  - name: "Improved sorting and filtering for projects and groups in Your Work"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/working_with_projects.html#search-in-projects'
    reporter: lohrc
    stage: data_stores
    categories:
    - Groups & Projects
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/25368'
    force_left: true
    description: |
      We have updated the sorting and filtering functionality of the project and group overview in **Your Work**.
      Previously, in the **Your Work** page for projects, you could filter by name and language, and use a pre-defined set of sorting options. We have standardized the sorting options to include **Name**, **Created date**, **Updated date**, and **Stars**. We also added a navigation element to sort in ascending or descending order, and moved the language filter to the filter menu. Now you can find archived projects in the new **Inactive** tab. Additionally, we added a **Role** filter that allows you to search for projects you are the Owner of.

      In the Your Work page for groups, we have standardized the sorting options to include **Name**, **Created date**, and **Updated date**, and added a navigation element to sort in ascending or descending order.

      We welcome feedback about these changes in [#438322](https://gitlab.com/gitlab-org/gitlab/-/issues/438322).
