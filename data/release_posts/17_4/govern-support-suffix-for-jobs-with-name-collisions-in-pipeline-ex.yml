---
features:
  secondary:
  - name: "Support suffix for jobs with name collisions in pipeline execution policy
      pipelines"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html#pipeline-execution-policy-schema'
    reporter: g.hickman
    stage: software_supply_chain_security
    categories:
    - Security Policy Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/473189'
    description: |
      An enhancement to the [17.2 release of pipeline execution policies](https://about.gitlab.com/releases/2024/07/18/gitlab-17-2-released/#pipeline-execution-policy-type), policy creators may now configure pipeline execution policies to handle collisions in job names gracefully. With the `policy.yml` for the pipeline execution policy, you may now configure the following options:

        - `suffix: on_conflict` configures the policy to gracefully handle collisions by renaming policy jobs, which is the new default behavior
        - `suffix: never` enforces all jobs names are unique and will fail pipelines if collisions occur, which has been the default behavior since 17.2

        With this improvement, you can ensure security and compliance jobs executed within a pipeline execution policy always run, while also preventing unnecessary impacts to developers downstream.

        In a follow-up enhancement, we will introduce the configuration option within the policy editor.
