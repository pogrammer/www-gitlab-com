---
features:
  secondary:
  - name: "Non-deployment jobs to protected environments aren't turned into manual jobs"
    available_in: [premium, ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/ci/jobs/job_control.html#types-of-manual-jobs'
    reporter: nagyv-gitlab
    stage: deploy
    categories:
    - Environment Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/390025'
    description: |
      Due to an implementation issue, the `action: prepare`, `action: verify`, and `action: access` jobs
      become manual jobs when they run against a protected environment. These jobs require manual interaction to run,
      although they don't require any additional approvals.

      [Issue 390025](https://gitlab.com/gitlab-org/gitlab/-/issues/390025) proposes to fix the implementation, so these jobs won't be turned into manual jobs.
      After this proposed change, to keep the current behavior, you will need to
      [explicitly set the jobs to manual](https://docs.gitlab.com/ee/ci/jobs/job_control.html#types-of-manual-jobs).

      For now, you can change to the new implementation now by enabling the `prevent_blocking_non_deployment_jobs` feature flag.

      Any proposed breaking changes are intended to differentiate the behavior of the
      `environment.action: prepare | verify | access` values.
      The `environment.action: access` keyword will remain the closest to its current behavior.

      To prevent future compatibility issues, you should review your use of these keywords now.
      You can learn more about these proposed changes in the following issues:

      - [Issue 437132](https://gitlab.com/gitlab-org/gitlab/-/issues/437132)
      - [Issue 437133](https://gitlab.com/gitlab-org/gitlab/-/issues/437133)
      - [Issue 437142](https://gitlab.com/gitlab-org/gitlab/-/issues/437142)
