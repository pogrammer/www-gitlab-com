features:
  primary:
  - name: "Auto-merge when all checks pass"
    available_in: [core, premium, ultimate]  # Include all supported tiers
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html'
    video: 'https://www.youtube-nocookie.com/embed/UbqAYizAFAk'
    reporter: phikai
    stage: create # Prefix this file name with stage-informative-title.yml
    stage_url:  '/stages-devops-lifecycle/'
    categories:
      - 'Code Review Workflow'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/gitlab-org/gitlab/-/issues/8128'
    epic_url:
      - 'https://gitlab.com/groups/gitlab-org/-/epics/10874'
    description: |
      Merge requests have many required checks that must pass before they are mergeable. These checks can include approvals, unresolved threads, pipelines, and other items that need to be satisfied. When you're responsible for merging code, it can be hard to keep track of all of these events, and know when to come back and check to see if a merge request can be merged.

      GitLab now supports **Auto-merge** for all checks in merge requests. Auto-merge enables any user who is eligible to merge to set a merge request to **Auto-merge**, even before all the required checks have passed. As the merge request continues through its lifecycle, the merge request automagically merges after the last failing check passes.

      We're really excited about this improvement to accelerate your merge request workflows. You can leave feedback about this feature in [issue 438395](https://gitlab.com/gitlab-org/gitlab/-/issues/438395).
