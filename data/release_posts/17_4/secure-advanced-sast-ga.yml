---
features:
  primary:
  - name: "Advanced SAST is generally available"
    available_in: [ultimate]
    gitlab_com: true
    add_ons: []
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html'
    image_url: '/images/17_4/secure-advanced-sast-code-flow.png'
    reporter: connorgilbert
    stage: application_security_testing
    categories:
      - 'SAST'
    issue_url: # Multiple links are supported. Avoid linking to confidential issues.
      - 'https://gitlab.com/gitlab-org/gitlab/-/issues/466322'
    description: |
      We're excited to announce that our Advanced Static Application Security Testing (SAST) scanner is now generally available for all GitLab Ultimate customers.

      Advanced SAST is a new scanner powered by the technology we [acquired from Oxeye](https://about.gitlab.com/blog/2024/03/20/oxeye-joins-gitlab-to-advance-application-security-capabilities/) earlier this year. It uses a proprietary detection engine with rules informed by in-house security research to identify exploitable vulnerabilities in first-party code. It delivers more accurate results so developers and security teams don't have to sort through the noise of false-positive results.

      Along with the new scanning engine, GitLab 17.4 includes:

      - A new [code-flow view](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#vulnerability-code-flow) that traces a vulnerability's path across files and functions.
      - An automatic migration that allows Advanced SAST to "take over" existing results from previous GitLab SAST scanners.

      To learn more, see [the announcement blog](https://about.gitlab.com/blog/2024/09/19/gitlab-advanced-sast-is-now-generally-available).
