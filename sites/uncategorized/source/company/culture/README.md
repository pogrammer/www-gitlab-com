# Culture has been migrated

This handbook content has been migrated to the new handbook site and as such this directory
has been locked from further changes.

Viewable content: [https://handbook.gitlab.com/handbook/company/culture](https://handbook.gitlab.com/handbook/company/culture)
Repo Location: [https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/culture](https://gitlab.com/gitlab-com/content-sites/handbook/-/tree/main/content/handbook/company/culture)

If you need help or assistance with this, please reach out on Slack in
[`#handbook`](https://gitlab.slack.com/archives/C81PT2ALD), or to the
[current handbook DRI](https://handbook.gitlab.com/handbook/about/maintenance/).

