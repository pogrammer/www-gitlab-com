---
title: "GitLab Security Release: 16.9.2, 16.8.4, 16.7.7"
categories: releases
author: Greg Myers
author_gitlab: greg
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.9.2, 16.8.4, 16.7.7 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/03/06/security-release-gitlab-16-9-2-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---


Today we are releasing versions 16.9.2, 16.8.4, 16.7.7 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Bypassing CODEOWNERS approval allowing to steal protected variables](#bypassing-codeowners-approval-allowing-to-steal-protected-variables) | High |
| [Guest with manage group access tokens can rotate and see group access token with owner permissions](#guest-with-manage-group-access-tokens-can-rotate-and-see-group-access-token-with-owner-permissions) | Medium |

### Bypassing CODEOWNERS approval allowing to steal protected variables

An authorization bypass vulnerability was discovered in GitLab affecting versions 11.3 prior to 16.7.7, 16.7.6 prior to 16.8.4, and 16.8.3 prior to 16.9.2. An attacker could bypass CODEOWNERS by utilizing a crafted payload in an old feature branch to perform malicious actions.
This is a high severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:H/I:H/A:N`, 7.7).
It is now mitigated in the latest release and is assigned [CVE-2024-0199](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-0199).

Thanks [ali_shehab](https://hackerone.com/ali_shehab) for reporting this vulnerability through our HackerOne bug bounty program.


### Guest with manage group access tokens can rotate and see group access token with owner permissions

A privilege escalation vulnerability was discovered in GitLab affecting versions 16.8 prior to 16.8.4 and 16.9 prior to 16.9.2. It was possible for a user with custom role of `manage_group_access_tokens` to rotate group access tokens with owner privileges.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:H/A:N`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-1299](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1299).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


### Upgrade Kubectl to the latest stable version

`kubectl` has been updated to version 1.29.2.


### Mattermost Security Updates February 14, 2024

Mattermost has been updated to version 9.5, which contains several patches and security fixes.


## Non Security Patches

### 16.9.2

* [Merge branch 'hm-rescue-stale-element-error-in-base' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146113)
* [Fix broken master](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146033)
* [Use fixed date for failing specs [16.9]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146202)
* [Backport 'pb-fix-broken-master-elastic' into 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146031)
* [Backport Fix Search::Zoekt.index? logic to 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145946)
* [Backport 'Don't escape search term in modal twice' into 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146036)
* [Backport 'add-praefect-to-release-environment-template'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146315)
* [Backport 'Shows branch name in non-blob...scopes'  into 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146035)
* [Backport: Geo - Fix container repositories checksum mismatch errors](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146181)
* [Backport 145801 (Fix CI linter error when repository is empty) to 16.9](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146074)
* [Merge branch 'remove-pi-os-12-release' into 'master'](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7444)
* [Backport to 16.9: Fix Geo: Personal snippets not syncing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145036)

### 16.8.4

* [Backport to 16.8: Fix Geo: Personal snippets not syncing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145037)
* [Backport to 16.8: Fix pg_dump failing with multiple PG read-replicas](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145451)
* [Update tests for broken 16.8](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146233)
* [Backport 'add-praefect-to-release-environment-template'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146316)
* [Backport: Geo - Fix container repositories checksum mismatch errors](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146183)
* [Backport 145801 (Fix CI linter error when repository is empty) to 16.8](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146073)

### 16.7.7

* [Backport to 16.7: Fix pg_dump failing with multiple PG read-replicas](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/145569)
* [Merge branch 'add-praefect-to-release-environment-template'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146318)

## Updating

To update GitLab, see the [Update page](/update).
To update GitLab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
