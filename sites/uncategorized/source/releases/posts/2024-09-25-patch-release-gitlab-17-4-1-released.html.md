---
title: "GitLab Patch Release: 17.4.1, 17.3.4, 17.2.8"
categories: releases
author: Greg Myers
author_gitlab: greg
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.4.1, 17.3.4, 17.2.8 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/09/25/patch-release-gitlab-17-4-1-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.4.1, 17.3.4, 17.2.8 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below be **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Maintainer can leak Dependency Proxy password by changing Dependency Proxy URL via crafted POST request](#maintainer-can-leak-dependency-proxy-password-by-changing-dependency-proxy-url-via-crafted-post-request) | Medium |
| [AI feature reads unsanitized content, allowing for attacker to hide prompt injection](#ai-feature-reads-unsanitized-content-allowing-for-attacker-to-hide-prompt-injection) | Low |
| [Project reference can be exposed in system notes](#project-reference-can-be-exposed-in-system-notes) | Low |

### Maintainer can leak Dependency Proxy password by changing Dependency Proxy URL via crafted POST request

An information disclosure issue has been discovered in GitLab EE affecting all versions starting from 16.5 prior to 17.2.8, from 17.3 prior to 17.3.4, and from 17.4 prior to 17.4.1. A maintainer could obtain a Dependency Proxy password by editing a certain Dependency Proxy setting via a POST request.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:L/I:L/A:N), 5.5).
It is now mitigated in the latest release and is assigned [CVE-2024-4278](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4278).

Thanks [ac7n0w](https://hackerone.com/ac7n0w) for reporting this vulnerability through our HackerOne bug bounty program.


### AI feature reads unsanitized content, allowing for attacker to hide prompt injection

An issue has been discovered in GitLab EE affecting all versions starting from 16.0 prior to 17.2.8, from 17.3 prior to 17.3.4, and from 17.4 prior to 17.4.1. An AI feature was found to read unsanitized content in a way that could've allowed an attacker to hide prompt injection.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:N/I:L/A:N), 3.1).
It is now mitigated in the latest release and is assigned [CVE-2024-4099](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4099).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Project reference can be exposed in system notes

An information disclosure issue has been discovered in Gitlab EE/CE affecting all versions from 15.6 prior to 17.2.8, 17.3 prior to 17.3.4, and 17.4 prior to 17.4.1. In specific conditions it was possible to disclose the path of a private project to an unauthorized user.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:N), 2.6).
It is now mitigated in the latest release and is assigned [CVE-2024-8974](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8974).

This vulnerability has been discovered internally by GitLab team member [Lukas Eipert](https://gitlab.com/leipert).


### Mattermost Security Updates August 27, 2024

Mattermost has been updated to version 9.11.1, which contains several patches and security fixes.

## Bug fixes


### 17.4.1

* [Improve OpenSSL callout message](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166427)
* [Change urgency of API project/:id/share to `low`](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166564)
* [Check commit message for issue close pattern setting](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166363)
* [Backport: Fixes issues with incorrectly displaying VR button](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166540)
* [Backport 'Fix incorrect gitlab-shell-check filename' into 17.4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166694)
* [Update OpenSSL v3 callout to delay update to GitLab 17.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166933)

### 17.3.4

* [Improve OpenSSL callout message](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166181)
* [Fix Code Review AI features policies to check duo features enabled toggle](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166302)
* [Update OpenSSL v3 callout to delay update to GitLab 17.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166934)

### 17.2.8

* [Improve OpenSSL callout message](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166183)
* [Update OpenSSL v3 callout to delay update to GitLab 17.7](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166935)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
