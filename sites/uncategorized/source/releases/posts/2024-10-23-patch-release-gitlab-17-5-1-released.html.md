---
title: "GitLab Patch Release: 17.5.1, 17.4.3, 17.3.6"
categories: releases
author: Kevin Morrison
author_gitlab: kmorrison1
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.5.1, 17.4.3, 17.3.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/10/23/patch-release-gitlab-17-5-1-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.5.1, 17.4.3, 17.3.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [HTML injection in Global Search may lead to XSS](#html-injection-in-global-search-may-lead-to-xss) | High |
| [DoS via XML manifest file import](#dos-via-xml-manifest-file-import) | Medium |

### HTML injection in Global Search may lead to XSS

An issue has been discovered in GitLab CE/EE affecting all versions from 15.10 before 17.3.6, 17.4 before 17.4.3, and 17.5 before 17.5.1. An attacker could inject HTML into the Global Search field on a diff view leading to XSS.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N), 8.7).
It is now mitigated in the latest release and is assigned [CVE-2024-8312](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8312).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### DoS via XML manifest file import

An issue has been discovered in GitLab CE/EE affecting all versions from 11.2 before 17.3.6, 17.4 before 17.4.3, and 17.5 before 17.5.1. A denial of service could occur via importing a malicious crafted XML manifest file.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-6826](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6826).

Thanks [a92847865](https://hackerone.com/a92847865) for reporting this vulnerability through our HackerOne bug bounty program.


### Update regarding helm charts, devkit and analytics stack

`Helm charts`, `devkit` and `analytics stack` have been patched to no longer support dynamic funnels.


### Bump Ingress NGINX Controller image to 1.11.2

The GitLab chart bundles a forked Ingress NGINX Controller subchart. We've updated its image version to 1.11.2.


## Bug fixes


### 17.5.1

* [Security patch upgrade alert: Only expose to admins](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170051)
* [Backport: Ensure postgresql_new is included in GitLab CE](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7995)

### 17.4.3

* [Resolve "UBI FIPS: Error in bashrc due to hardening script" (17.4)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2058)
* [Backport: fix: Allow non-root user to run the bundle-certificates script 17.4](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2044)
* [Backport gocloud.dev update to 17.4](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7351)
* [Backport bundle fetch fsck fix to 17.4](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7362)
* [Backport Stable Branch Danger Checks to 17-4-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168691)
* [Add version to pdf.js file in webpack builds](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168669)
* [Backport: Skip rspec fail-fast jobs if pipeline:skip-rspec-fail-fast label is set](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168793)
* [Backport fix Zoekt global code search](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168569)
* [Set author on issuable to current user if it is not already set](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168776)
* [Backport LabKit v1.21.2 update to fix broken dependency](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169190)
* [Fix broken duo chat spec after free access cutoff [17.4]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169625)
* [Backport: Ensure postgresql_new is included in GitLab CE](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7998)

### 17.3.6

* [Resolve "UBI FIPS: Error in bashrc due to hardening script" (17.3)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2057)
* [Backport CreateRepositoryFromURL error handling to 17.3](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7339)
* [Set author on issuable to current user if it is not already set](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168938)
* [Fix broken duo chat spec after free access cutoff [17.3]](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/169627)
* [Backport Stable Branch Danger Checks to 17-3-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168818)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
